#'  Remove identifier from SACT data
#'
#'  This function removes the NHS Number in the SACT data.  This is not data encyrption.  You could consider using a package such as
#'  'encryptr' package to encrypt the data (but it can become very large).  Instead we simply are replacing the NHS Number with
#'  a 'case_id'.  You have the choice to keep a key linking the case_id and the NHS Number.
#'
#'  There are a number of reasons to choose to do this, but the implications also need to be understood.  Removing the NHS Number
#'  number means there is far lower risk of data loss.  However, it makes joining data, or updating data (e.g. with new Date of Death)
#'  impossible.  To get around this, we offer to store a key to translate the  'case_id' back to NHS Number.  Of course making that possible
#'  means if someone obtains the key, they can do the same.
#'
#'  Note: even without NHS Number there are other very identifiable fields (Date of Birth and Post Code for example)
#'
#' @param data A valid SACT data table or tibble with NHS_Number as a data column
#' @param lookup A logical value (true or false) to indicate if a key should be stored to allow re-identification
#' @param lookup_name A quoted character string naming the lookup (this will be created as an R environment object with this name). If write_lookup is True, this will also be used to name the file
#' @param write_lookup A logical indicating if the lookup should be written to a CSV file with the same lookup name
#' @param update_lookup A logical indicating if an existing lookup exists. If True, then new cases are added to the existing lookup rather than recreating a new lookup.  This will require that a CSV file exists with the correct name
#' @return an de-identified SACT dataset with or without a translation key
#' @examples
#'
#' # Encrypt SACT data, returning a key
#' \dontrun{
#' remove.identifier.sact(mySACTdata, lookup = TRUE)
#' }
#'
#' @export remove.identifier.sact



remove.identifier.sact <- function(.data , lookup = FALSE,
                                   lookup_name = "lookup", write_lookup = TRUE, update_lookup = TRUE) {

    .filename = paste0(lookup_name, ".csv")
    if (update_lookup) {
        if(file.exists(.filename)) {
            .lookup <- read.csv(.filename)
            .max_Case_id <- max(.lookup$Case_id, na.rm = T)
        } else {
            warning (glue::glue("No lookup file - {.filename} - was found. Creating a fresh lookup table."))
            .lookup <- tibble(NHS_Number = character(), Case_id = integer())
            .max_Case_id = 0
        }



        .data |>
            select(NHS_Number) |>
            unique() |>
            left_join(.lookup) |>
            arrange(Case_id) |>
            filter (is.na(Case_id) ) |>
            mutate (Case_id = row_number()+.max_Case_id) %>%
            bind_rows(.lookup, .) -> .lookup

    } else {

        .data |>
            select(NHS_Number) |>
            unique() |>
            mutate(Case_id = row_number()) -> .lookup
    }

    .data |>
        left_join(.lookup) |>
        relocate(Case_id, .after = NHS_Number) |>
        select(-NHS_Number) -> .data

    if (lookup) {
        assign(lookup_name, .lookup)
    }

    if (write_lookup) {
        write.csv(.lookup, paste0(lookup_name, ".csv"), row.names = F)
    }

    return(.data)

}


