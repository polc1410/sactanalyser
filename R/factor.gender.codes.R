#' Translate the Current Gender Codes into a Readable Gender
#'
#' This function will look up the NHS Gender Codes (if online access is available) and factor the Current Gender Column.
#' If online access is unavailable, it will be factored as 1 = Male, 2 = Female, 9 = Indeterminate (unable to be classified as either male or female), X = Not kknown
#'
#'
#

#' @param x A SACT data frame.
#' @return The SACT data frame with Current Gender Column refactored
#' @examples
#' \dontrun{
#' factor.gender.codes(x)
#' }
#' @export factor.gender.codes

#require(NHSDataDictionaRy)

factor.gender.codes <- function(x) {

    if (!exists("x")) {
        stop("No data frame was provided")
    }

    # Get the national NHSdictionary
    NHSdict <- NHSDataDictionaRy::nhs_table_findeR("PERSON STATED GENDER CODE", title="National Codes") %>%
        add_row(Code = "X", Description  = "Not known")

    if (is.null(NHSdict)) {
        # Unable to lookup - code manually
        NHSdict <- tibble( Code = c(1,2,9,"X"), Description = c("Male", "Female", "Indeterminate", "Not known"))
    }

    x %>%
        mutate(Gender_current = factor(
            Gender_current,
            levels = NHSdict$Code,
            labels = NHSdict$Description

        )) -> x


    return(x)

}


