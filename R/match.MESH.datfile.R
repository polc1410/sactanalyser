#' @title Import the MESH dat file
#'
#' Once you have received the dat file back from NHS Digital you can exclude data by matching the file with your SACT dataset.
#'
#' The result will report the numerical number of patients who were excluded from your data but not their identity.
#'
#' @param x A SACT data frame. This should be the same frame used to create the dat file sent to NHS Digital
#' @param filename The dat file returned by NHS Digital
#' @return The matched i.e. filtered data frame and a number of patient IDs the were unmatched
#' @examples
#' \dontrun{
#' match.MESH.datfile(mySACTData, "rSACT-YYYYMMDD-001.dat") -> mySACTData
#' }
#' # There were 372095 rows of data from 15969 patients.
#' # After NHS Data Opt-Out was applied there were 238840 rows of data from 10175 patients.
#' # Data for 5794 patients was suppressed, this should be reported in any publication.
#'
#' @export match.MESH.datfile



match.MESH.datfile <- function(x, filename) {

    require(magrittr)

    if (!exists("x")) {
        stop("No data frame was provided")
    }

    if (!exists("filename")) {
        stop("No dat file was provided")
    }

    matches <- read.table(filename,  col.names = c("NHS_Number","Data_Opt_Out"), sep=",")
    matches$Data_Opt_Out <- F

    matches$NHS_Number <- as.character(matches$NHS_Number)

    len_x <- length(x$NHS_Number)
    len_Ux <- length(unique(x$NHS_Number))

    message (glue::glue("There were {len_x} rows of data from {len_Ux} patients."))

    x %>%
        dplyr::inner_join(matches, by ="NHS_Number")   %>%
        dplyr::select(-`Data_Opt_Out`) -> x

    len_xy <- length(x$NHS_Number)
    len_Uxy <- length(unique(x$NHS_Number))

    message (glue::glue("After NHS Data Opt-Out was applied there were {len_xy} rows of data from {len_Uxy} patients."))
    message (glue::glue("Data for {len_Ux - len_Uxy} patients was suppressed, this should be reported in any publication."))

    return(x)

}

