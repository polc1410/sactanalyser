#' Update censor dates using a variety of sources
#'
#' This function will update the censor dates (i.e. date patient last confirmed alive).  This can be done in a number of ways:
#'
#' 1. Using last treatment administration date in the SACT files, this will normally be the least up to date approach
#' 2. Using an external 'free-standing' data-source such as a CSV file of patients and dates confirmed alive, the data can be updated
#' 3. Using database connections the data can be updated.  This currently supports
#'    a. The CAMIS PMI (provided there is a database connection available), using the Informix Drivers.
#'    This methods are specific to a single installation, but the function could be expanded to accommodate more options if there is use.
#'


#' @param x A SACT data frame.
#' @param filename A filename (including path if not the working directory) of a CSV file, containing at least a column 'NHS_number' and a column 'Date_of_death' (in 'YYYY-MM-DD' format), which will be used for updates.
#' @param database The name of a database (e.g. chemolive for chemocare). Currently valid options are 'chemolive' and 'camis'
#' @param Server a resolvable domain name or IP address identifying the server location of the database
#' @param DSN A 'Data Source Name' database definition. Server, UID and PWD will not be required for this
#' @param UID a username for the database connection. For security reasons it would be preferable to not pass username and password in free text.  If the keyring package is installed username and password will be taken from there if they exist
#' @param PWD a password for the database connection.
#'
#'
#' @return The SACT data frame with Date of Death updated
#' @examples
#' # Update dates last alive using the most recent SACT data (this is NOT reliable or complete)
#' \dontrun{
#' update.censor.date(sData)
#'
#'
#' # This will update censor dates from the CAMIS master patient index using a DSN to connect
#' update.censor.date(sData, database="camis", DSN="CAMISDRPMI")
#' # Alternatively
#' update.censor.date(sData, database="camis", Server="Camis_Server", UID="camis_user", PWD = "camis_pass")
#' }
require(dplyr)
require(dbplyr)
require(DBI)

update.censor.date <- function(x, filename="", database="", Server = "", UID = "", PWD = "", DSN = "") {

    if (!exists("x")) {
        stop("No data frame was provided")
    }

    if (filename == "" & database == "") {
        # create a list of censor Dates using the last administratioin date
        # using last date on file
        # then update back to system

        x |>
            group_by(NHS_Number) |>
            summarise(Censor_date = max(Administration_date)) -> censorList

        x |>
            left_join(censorList) -> x


    }

    if (database == "camis" ) {
        #
        # Connect to the camis database and retrieve Date of last attended appointment from
        #
        # This can be slow.  It makes sense to do it incrementally rather than ask for 2k patients at once


        if (Server == "" & DSN == "") {
            stop("You MUST specify the server name or IP address to use a database, OR the DSN.")
        }

        if (DSN != "") {

            if (UID == "") {
                if (!"keyring" %in% installed.packages()[,"Package"]) {
                    warning("You have not specified a userid (UID) for the database.  This may not be essential, but may result in a connection failure.")
                } else {
                    if (length ( keyring::key_list("camis")$username) == 1) {
                        UID <- keyring::key_list("camis")$username
                    } else {
                        warning(
                            glue::glue("There is not a unique username set in the keyring system. If no username is required for database connection this warning can be ignored.  If a username is required, specify it in the command line with UID='username'. If there are multiple usernames in the keyring, specify which to use.  Add usernames and passwords to the keyring with: keyring::key_set('{database}', username)")
                        )
                    }
                }
            }

            if (PWD == "" & DSN == "") {
                if ("keyring" %in% installed.packages()[,"Package"]) {
                    PWD <-  keyring::key_get(database, UID)
                    } else {
                        warning("You have not specified a password for the database.  This may not be essential, but may result in a connection failure.")


                }
            }

            con <-  dbConnect(odbc::odbc(), DSN = DSN, timeout = 10, dbname = "camis")

        } else {
            con <- dbConnect(odbc::odbc(), Server = Server,
                             dbname = database, UID = UID, PWD = PWD,
                             timeout = 10)
        }

    # Add a column for Censor Date
    x$Censor_date = NA

    message ("Updating censor dates. This can take some time.")
    pb <- txtProgressBar(min = 0,      # Minimum value of the progress bar
                         max = length(unique(x$NHS_Number)), # Maximum value of the progress bar
                         style = 3,    # Progress bar style (also available style = 1 and style = 2)
                         width = 50,   # Progress bar width. Defaults to getOption("width")
                         char = "=")   # Character used to create the bar

    progressCounter = 0


    for (CaseID in unique(x$NHS_Number)) {
        try(x$Censor_date[x$NHS_Number == CaseID] <- CamisLastAttendance(CaseID, con))
        prgressCounter = progressCounter + 1
        setTxtProgressBar(pb, progressCounter)
    }

    close(pb) # Close the progress bar

    }


    return(x)

}


