#' Calculate age at start of treatment
#'
#' This function will calculate the age of the patient at the start of treatment
#'
#' @param x A SACT datatable
#'
#' @return A SACT datatable with an additional column - Age_at_regimen_start
#'
#' @export calculate.age
#'
#' @examples
#' \dontrun{
#' calculate.age(mySACTdata)
#' }
#'



calculate.age <- function(x) {
    require(lubridate)
    require(magrittr)

 if (!exists("x")) {
        stop("No SACT dataset was provided")
    }


    x %>%
        dplyr::mutate(
            Age_at_regimen_start = trunc((Date_of_birth %--% Start_date_of_regimen) / lubridate::years(1))
        ) -> x

    return(x)

}


