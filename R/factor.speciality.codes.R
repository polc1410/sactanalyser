#' Translate the Ethnicity Codes into a Readable Ethnicity
#'
#' This function will look up the NHS Ethnicity Codes (if online access is available) and factor the Ethnicity Column.
#' If online access is unavailable, it will be factored as
#'
#' Note: Ethnicity is not present in SACT v3
#

#' @param x A SACT data frame.
#' @return The SACT data frame with Ethnicity Column refactored
#' @examples
#' \dontrun{
#' factor.ethnicity.codes(x)
#' }

#require(NHSDataDictionaRy)

factor.ethnicity.codes <- function(x) {

    if (!exists("x")) {
        stop("No data frame was provided")
    }

    # Get the national NHSdictionary
    NHSdict <- NHSDataDictionaRy::nhs_table_findeR("ETHNIC CATEGORY", title="National Codes")

    if (is.null(NHSdict)) {
        # Unable to lookup - code manually
        NHSdict <- tibble(
            Code = c(LETTERS[1:16],"Z"),
            Description = c('White - British',
            'White - Irish',
            'White - Any other White background',
            'Mixed - White and Black Caribbean',
            'Mixed - White and Black African',
            'Mixed - White and Asian',
            'Mixed - Any other mixed background',
            'Asian or Asian British - Indian',
            'Asian or Asian British - Pakistani',
            'Asian or Asian British - Bangladeshi',
            'Asian or Asian British - Any other Asian background',
            'Black or Black British - Caribbean',
            'Black or Black British - African',
            'Black or Black British - Any other Black background',
            'Other Ethnic Groups - Chinese',
            'Other Ethnic Groups - Any other ethnic group',
            'Not stated') )
    }

    x %>%
        mutate(Ethnicity = factor(
            Ethnicity,
            levels = NHSdict$Code,
            labels = NHSdict$Description

        )) -> x


    return(x)

}


