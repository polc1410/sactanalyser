#' Update dates of death using a variety of sources
#'
#' This function will update the date of death.  This can be done in a number of ways:
#'
#' 1. Using date of death in the SACT files, the function can update older SACT data with the date of death
#' 2. Using an external 'free-standing' data-source such as a CSV file of patients and dates of death, the data can be updated
#' 3. Using database connections the data can be updated.  This currently supports
#'    a. The CAMIS PMI (provided there is a database connection available), using the Informix Drivers.
#'    b. A SQL Lookup into ChemoCare database.
#'    Both these methods are specific to a single installation, but the function could be expanded to accommodate more options if there is use.
#'


#' @param x A SACT data frame.
#' @param filename A filename (including path if not the working directory) of a CSV file, containing at least a column 'NHS_number' and a column 'Date_of_death' (in 'YYYY-MM-DD' format), which will be used for updates.
#' @param database The name of a database (e.g. chemolive for chemocare). Currently valid options are 'chemolive' and 'camis'
#' @param Server a resolvable domain name or IP address identifying the server location of the database
#' @param DSN A 'Data Source Name' database definition. Server, UID and PWD will not be required for this
#' @param UID a username for the database connection. For security reasons it would be preferable to not pass username and password in free text.  If the keyring package is installed username and password will be taken from there if they exist
#' @param PWD a password for the database connection.
#'
#'
#' @return The SACT data frame with Date of Death updated
#' @examples
#' \dontrun{
#' # Update mortality using the most recent SACT data (this is NOT reliable or complete)
#' update.mortality(sData)
#'
#' # Update mortality using date of death from a ChemoCare SQL Server called NHS_ChemoCare
#' # Provided the HL7 interfaces are set up correctly and the SACT data is sourced from the
#' # same server, this should be complete.
#' #
#' # This will use they password from the keyring systme for the user chemolive
#' update.mortality(sData, database="chemolive", Server="NHS_ChemoCare", UID="chemolive")
#'
#' # This will update mortality from the CAMIS master patient index using a DSN to connect
#' update.mortality(sData, database="camis", DSN="CAMISDRPMI")
#' }
#'
#'@export update.mortality
#'
require(dplyr)
require(DBI)

update.mortality <- function(x, filename="", database="", Server = "", UID = "", PWD = "", DSN = "") {

    if (!exists("x")) {
        stop("No data frame was provided")
    }

    if (filename == "" & database == "") {
        # create a list of dates of death for all patients
        # using last date on file
        # then update back to system

        x |>
            group_by(NHS_Number) |>
            summarise(Date_of_death = last(Date_of_death)) -> deathList


    }

    if (database == "chemolive" ) {
        #
        # Connect to the chemocare database and retrieve Date of deaths from
        #

        if (Server == "") {
            stop("You MUST specify the server name or IP address to use a database.")
        }

        if (UID == "") {
            if (!"keyring" %in% installed.packages()[,"Package"]) {
                warning("You have not specified a userid (UID) for the database.  This may not be essential, but may result in a connection failure.")
            } else {
                if (length ( keyring::key_list("chemolive")$username) == 1) {
                    UID <- keyring::key_list("chemolive")$username
                } else {
                    warning(
                        glue::glue("There is not a unique username set in the keyring system. If no username is required for database connection this warning can be ignored.  If a username is required, specify it in the command line with UID='username'. If there are multiple usernames in the keyring, specify which to use.  Add usernames and passwords to the keyring with: key_set('{database}', username)")
                    )
                }
            }
        }

        if (PWD == "" & DSN == "") {
            if ("keyring" %in% installed.packages()[,"Package"]) {
                PWD <-  keyring::key_get(database, UID)
                } else {

                    warning("You have not specified a password for the database.  This may not be essential, but may result in a connection failure.")
            }
        }

        con <- dbConnect(odbc::odbc(), Driver = "SQL Server", Server = Server,
                         Database = database, UID = UID, PWD = PWD,
                         timeout = 10)

        deathList <- tbl(con,"PMI") |>
            filter(DEATH_DATE != "") |>
            select(NHS_Number = DISTRICTNO, Date_of_death = DEATH_DATE) |>
            collect()

    }


    if (database == "camis" ) {
        #
        # Connect to the camis database and retrieve Date of deaths from
        #

        if (Server == "" & DSN == "") {
            stop("You MUST specify the server name or IP address to use a database, OR the DSN.")
        }

        if (DSN != "") {

            if (UID == "") {
                if (!"keyring" %in% installed.packages()[,"Package"]) {
                    warning("You have not specified a userid (UID) for the database.  This may not be essential, but may result in a connection failure.")
                } else {
                    if (length ( keyring::key_list("camis")$username) == 1) {
                        UID <- keyring::key_list("camis")$username
                    } else {
                        warning(
                            glue::glue("There is not a unique username set in the keyring system. If no username is required for database connection this warning can be ignored.  If a username is required, specify it in the command line with UID='username'. If there are multiple usernames in the keyring, specify which to use.  Add usernames and passwords to the keyring with: keyring::key_set('{database}', username)")
                        )
                    }
                }
            }

            if (PWD == "" & DSN == "") {
                if ("keyring" %in% installed.packages()[,"Package"]) {
                    PWD <-  keyring::key_get(database, UID)
                    } else {
                        warning("You have not specified a password for the database.  This may not be essential, but may result in a connection failure.")


                }
            }

            con <-  dbConnect(odbc::odbc(), DSN = DSN, timeout = 10, dbname = "camis")

        } else {
            con <- dbConnect(odbc::odbc(), Server = Server,
                             dbname = database, UID = UID, PWD = PWD,
                             timeout = 10)
        }

        deathList <- tbl(con,"pmi_patient") |>
            filter(!is.na(ppt_dateofdeath)) |>
            select(NHS_Number = pmi_link, Date_of_death = ppt_dateofdeath) #|> collect()

        pmi_patient <- tbl(con, "pmi_patient")
        pmi_numberlinks <- tbl(con, "pmi_numberlinks") |>
            filter (pnt_link == -4)

        caseList <- unique(x$NHS_Number)

        deathList <- pmi_patient |>
            left_join(pmi_numberlinks) |>
            select(NHS_Number = pno_number, Date_of_death = ppt_dateofdeath) |>
            filter(NHS_Number %in% caseList) |>
            collect() |>
            #For some reason these return lower case, so fix them
            rename(NHS_Number = nhs_number, Date_of_death = date_of_death)


    }

    if (filename != "") {
        deathList <- read.csv(filename, colClasses = c("character", "Date") )
    }


    x |>
        left_join(deathList, by="NHS_Number") |>
        select(-Date_of_death.x) |>
        rename(Date_of_death = Date_of_death.y) -> x

    return(x)

}


