# SACTanalyseR

Every NHS Trust within England that provides chemotherapy services collects and monthly submits a mandatory dataset to NHS Digital.

This package provides tools to help trusts manipulate those files for local analysis.

To be useful, you will require access to the raw files your trust uploads to NHS Digital.

**This package is in a very early beta phase.** Please report [issues](https://gitlab.com/polc1410/sactanalyser/-/issues) via our GitLab site.

This package is being developed with support from the [NHS R Community](https://nhsrcommunity.com/).

To install the package:

    install.packages("remotes")
    remotes::install_gitlab("polc1410/SACTanalyseR")
