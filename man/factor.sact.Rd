% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/factor.sact.R
\name{factor.sact}
\alias{factor.sact}
\title{Factor the a variety of coded elements in a SACT data frame}
\usage{
factor.sact(x)
}
\arguments{
\item{x}{A SACT data frame.}
}
\value{
The SACT data frame with factored and named elements added
}
\description{
This function will convert a number of coded elements to factors for easier reading of the data. It will also add additional
named columns where the use of the code may remain desirable.
}
